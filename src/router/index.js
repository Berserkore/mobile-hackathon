import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Nasser from '@/components/Nasser'
import NasStore from '@/components/NasStore'
import Karen from '@/components/Karen'
import Alvin from '@/components/Alvin'
import Brandon from '@/components/Brandon'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/nasser',
      name: 'Nasser',
      component: Nasser
    },
    {
      path: '/nasstore',
      name: 'nasstore',
      component: NasStore
    },
    {
      path: '/karen',
      name: 'Karen',
      component: Karen
    },
    {
      path: '/alvin',
      name: 'Alvin',
      component: Alvin
    },
    {
      path: '/brandon',
      name: 'Brandon',
      component: Brandon
    }
  ]
})
